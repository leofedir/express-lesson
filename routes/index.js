/* routes/users.js */

var express = require('express');
var router = express.Router();

const { getAllNames, getName, delName, saveName, updateRecord } = require("../services/user.service");

router.get('/user', function(req, res) {
  res.send(getAllNames());
});

router.get('/user/:id', function(req, res,) {
  res.send(getName(req.url.substring(6)));
});

router.delete('/user/:id', function(req, res) {
  res.send(delName(req.url.substring(6)));
});

router.post('/user', function(req, res) {
  res.send(saveName(req.body));
});

router.put('/user/:id', function(req, res) {
  res.send(updateRecord(req.url.substring(6),req.body));
});


module.exports = router;
