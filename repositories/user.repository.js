/* repositories/user.repository.js */
let jsonData = require('../datasource/mockDATA');

const getAllData = () => {
    console.log(jsonData);
    return jsonData;
}

const getNameById = (id) => {
    let id_num = parseInt(id);
    return jsonData.filter(item => parseInt(item.id) === id_num);
}

const deleteById = (id) => {
    let id_num = parseInt(id);
    jsonData = jsonData.filter(item => parseInt(item.id) !== id_num);
    return `User with id = ${id}. Was delete.`;
}

const saveData = (id,first_name,last_name,email) => {
    // код по сохранению данных в БД
    let obj = `,{"id":${id},"first_name":"${first_name}","last_name":"${last_name}","email":"${email}"}]`;
    let jsonStr = JSON.stringify(jsonData);
    jsonData = JSON.parse((jsonStr.substr(0,jsonStr.length-1) + obj));
    return `User ${id},${first_name},${last_name},${email} is saved`;
}

const updateData = (id, first_name,last_name,email) => {
    let idInt = parseInt(id);
    for (let item of jsonData) {
        if (parseInt(item.id) === idInt) {
            item.first_name = first_name;
            item.last_name = last_name;
            item.email = email;
            return `User with id=${id} updated to first_name = ${first_name}, last_name = ${last_name}, email=${email}`;
            break;
        }
    }
    return 'User not found';
}

module.exports = {
    getAllData, getNameById, deleteById, saveData, updateData
};
