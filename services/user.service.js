/* services/user.service.js */
const { getAllData, getNameById, deleteById, saveData, updateData} = require("../repositories/user.repository");
const regex = /^\d{1,4}$/;
const regexName = /^[a-zA-Z ]{1,50}$/;
const regexEmail = /^[^@]+@[^@]+\.[^@]+$/;

const getAllNames = () => {
    return getAllData();
}

const getName = (id) => {
    if (regex.test(id)) {
        return getNameById(id);
    } else {
        return null;
    }
};

const delName = (id) => {
    if (regex.test(id)) {
        return deleteById(id);
    } else {
        return null;
    }
};

const saveName = (user) => {
    if (user === null ||
        !user.hasOwnProperty('id') ||
        !user.hasOwnProperty('first_name') ||
        !user.hasOwnProperty('last_name') ||
        !user.hasOwnProperty('email') ||
        !regex.test(user.id) ||
        !regexName.test(user.first_name) ||
        !regexName.test(user.last_name) ||
        !regexEmail.test(user.email)
    ) {
        return null;
    } else {
        return saveData(user.id,user.first_name,user.last_name,user.email);
    }
};

const updateRecord = (id,user) => {
    if (!regex.test(id)||
        user === null ||
        !user.hasOwnProperty('first_name') ||
        !user.hasOwnProperty('last_name') ||
        !user.hasOwnProperty('email') ||
        !regexName.test(user.first_name) ||
        !regexName.test(user.last_name) ||
        !regexEmail.test(user.email)
    ) {
        return null;
    } else {
        return updateData(id, user.first_name,user.last_name,user.email);
    }
}

module.exports = {
    getAllNames,
    getName,
    delName,
    saveName,
    updateRecord
};
